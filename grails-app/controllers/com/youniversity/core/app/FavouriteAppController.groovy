package com.youniversity.core.app

import grails.transaction.*
import com.youniversity.core.user.User

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class FavouriteAppController {

    def permissions =[
            "/favourite-app/index":["configAttribute":"ROLE_USER_MEMBER"],
            "/favourite-app/create":["configAttribute":"ROLE_USER_MEMBER"],
            "/favourite-app/save/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/edit":["configAttribute":"ROLE_USER_MEMBER"],
            "/favourite-app/update/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/delete/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/show/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/list":["configAttribute":"ROLE_USER_MEMBER"],
            "/favourite-app/add/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/add-widget/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/remove/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/favourite-app/remove-widget/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ]
    ]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond FavouriteApp.list(params), model:[favouriteAppInstanceCount: FavouriteApp.count()]
    }

    def show(FavouriteApp favouriteAppInstance) {
        respond favouriteAppInstance
    }

    def create() {
        respond new FavouriteApp(params)
    }

    @Transactional
    def add(Integer id) {
        Application app = Application.findById(id)
        new FavouriteApp(application: app, user: User.get(1), widget:false).save()
        redirect controller: "Application", action: "index"
    }

    @Transactional
    def addWidget(Integer id) {
        Application app = Application.findById(id)
        new FavouriteApp(application: app, user: User.get(1), widget:true).save()
        redirect controller: "Application", action: "index"
    }

    @Transactional
    def remove(Integer id)  {
        Application app = Application.findById(id)
        User user = User.findById(1)
        FavouriteApp fav = FavouriteApp.findByApplicationAndUserAndWidget(app,user,false)
        fav.delete(flush:true)
        redirect controller: "Application", action: "index"
    }

    @Transactional
    def removeWidget(Integer id)  {
        Application app = Application.findById(id)
        User user = User.findById(1)
        FavouriteApp fav = FavouriteApp.findByApplicationAndUserAndWidget(app,user,true)
        fav.delete(flush:true)
        redirect controller: "Application", action: "index"
    }

    @Transactional
    def save(FavouriteApp favouriteAppInstance) {
        if(favouriteAppInstance.hasErrors()) {
            respond favouriteAppInstance.errors, view:'create'
        }
        else {
            favouriteAppInstance.save flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.created.message', args: [message(code: 'favouriteAppInstance.label', default: 'FavouriteApp'), favouriteAppInstance.id])
                    redirect favouriteAppInstance
                }
                '*' { respond favouriteAppInstance, [status: CREATED] }
            }
        }
    }


    def edit(FavouriteApp favouriteAppInstance) { 
        respond favouriteAppInstance  
    }

    @Transactional
    def update(FavouriteApp favouriteAppInstance) {
        if(favouriteAppInstance == null) {
            render status:404
        }
        else if(favouriteAppInstance.hasErrors()) {
            respond favouriteAppInstance.errors, view:'edit'
        }
        else {
            favouriteAppInstance.save flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'FavouriteApp.label', default: 'FavouriteApp'), favouriteAppInstance.id])
                    redirect favouriteAppInstance 
                }
                '*'{ respond favouriteAppInstance, [status: OK] }
            }
        }        
    }

    @Transactional
    def delete(FavouriteApp favouriteAppInstance) {
        if(favouriteAppInstance) {
            favouriteAppInstance.delete flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'FavouriteApp.label', default: 'FavouriteApp'), favouriteAppInstance.id])
                    redirect action:"index", method:"GET" 
                }
                '*'{ render status: NO_CONTENT }
            }                
        }
        else {
            render status: NOT_FOUND
        }
    }
}

