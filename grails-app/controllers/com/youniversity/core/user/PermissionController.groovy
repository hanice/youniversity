package com.youniversity.core.user



import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class PermissionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def permissions = [
            "/permission/index":["configAttribute":"ROLE_USER_MEMBER"]
    ]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Permission.list(params), model: [permissionInstanceCount: Permission.count()]
    }

    def show(Permission permissionInstance) {
        respond permissionInstance
    }

    def create() {
        respond new Permission(params)
    }

    @Transactional
    def save(Permission permissionInstance) {
        if (permissionInstance.hasErrors()) {
            respond permissionInstance.errors, view: 'create'
        } else {
            permissionInstance.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'permissionInstance.label', default: 'Permission'), permissionInstance.id])
                    redirect permissionInstance
                }
                '*' { respond permissionInstance, [status: CREATED] }
            }
        }
    }

    def edit(Permission permissionInstance) {
        respond permissionInstance
    }

    @Transactional
    def update(Permission permissionInstance) {
        if (permissionInstance == null) {
            render status: 404
        } else if (permissionInstance.hasErrors()) {
            respond permissionInstance.errors, view: 'edit'
        } else {
            permissionInstance.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Permission.label', default: 'Permission'), permissionInstance.id])
                    redirect permissionInstance
                }
                '*' { respond permissionInstance, [status: OK] }
            }
        }
    }

    @Transactional
    def delete(Permission permissionInstance) {
        if (permissionInstance) {
            permissionInstance.delete flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Permission.label', default: 'Permission'), permissionInstance.id])
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        } else {
            render status: NOT_FOUND
        }
    }
}

