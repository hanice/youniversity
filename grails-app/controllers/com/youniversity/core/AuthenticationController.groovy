package com.youniversity.core

class AuthenticationController {
    def authenticationService

    def beforeInterceptor = {
        def converter =  new grails.web.HyphenatedUrlConverter()

        def method = permissions?.getAt(converter.toUrlElement(actionUri))?.getAt("authMethod")?.getAt("ROLE_USER_MEMBER")
        if(grailsApplication?.getArtefact("Service","com.youniversity.core.AuthenticationService")?.metaClass?.methods?.find{it.name==method})
            if(!authenticationService?."${method}"())
                redirect controller: "login", action: "denied"

        //TODO N1 - loop through all user roles get their auth method, if true redirect to actionUri, if none pass redirect to denied
        //TODO N2 - add params
        //TODO N3 - add beforeInterceptor to parent class and extend it

    }
}
