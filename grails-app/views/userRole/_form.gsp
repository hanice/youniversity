<%@ page import="com.youniversity.core.user.UserRole" %>



<div class="fieldcontain ${hasErrors(bean: userRoleInstance, field: 'groupRole', 'error')} required">
    <label for="role">
        <g:message code="userRole.role.label" default="Role" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="groupRole" name="groupRole.id" from="${com.youniversity.core.user.GroupRole.list()}" optionKey="id" required="" value="${userRoleInstance?.groupRole?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userRoleInstance, field: 'user', 'error')} required">
    <label for="user">
        <g:message code="userRole.user.label" default="User" />
        <span class="required-indicator">*</span>
    </label>
    <g:select id="user" name="user.id" from="${com.youniversity.core.user.User.list()}" optionKey="id" required="" value="${userRoleInstance?.user?.id}" class="many-to-one"/>
</div>

