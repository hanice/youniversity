
<%-- BUGGED BECAUSE OF SPRING SECURITY'S BULLSHIT - See the task card for more info.

<%@ page import="com.youniversity.core.app.FavouriteApp; com.youniversity.core.user.User" %>

<g:set var="userObject" value="${User.get(1)}"/>  --%>
<%@ page import="com.youniversity.core.app.FavouriteApp" %>

<g:set var="userFavs" value="${FavouriteApp.findAll()}"/>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1" style="margin-top: 4em">
    <h3>Menu</h3>
    <input type="text" id="appsearch" />
    <div id="search-list"></div>
    <g:each in="${userFavs}" var="item">
        <g:if test="${!item?.widget}">
            <a href="/YOUniversity/app/${item?.application?.name}" class=""><g:message code="${item?.application?.displayedName}"/></a>
        </g:if>
    </g:each>
    <hr />
    <g:each in="${userFavs}" var="item">
        <g:if test="${item?.widget}">
            <a href="/YOUniversity/widget/${item?.application?.name}" class=""><g:message code="${item?.application?.displayedName}"/></a>
        </g:if>
    </g:each>

</nav>


<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
            menuRight = document.getElementById( 'cbp-spmenu-s2' ),
            menuTop = document.getElementById( 'cbp-spmenu-s3' ),
            menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
            showLeft = document.getElementById( 'showLeft' ),
            showRight = document.getElementById( 'showRight' ),
            showTop = document.getElementById( 'showTop' ),
            showBottom = document.getElementById( 'showBottom' ),
            showLeftPush = document.getElementById( 'showLeftPush' ),
            showRightPush = document.getElementById( 'showRightPush' ),
            body = document.body;

    showLeft.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeft' );
    };


    function disableOther( button ) {
        if( button !== 'showLeft' ) {
            classie.toggle( showLeft, 'disabled' );
        }
    }

    jQuery(document).ready(function($){
        $.ajax({
            type: "GET",
            url: "/YOUniversity/application/get-all-apps",
            success : function(applicationsList) {
                var appNames = $.map(applicationsList, function (app) {
                    return app.name
                });
                var apps = $.map(applicationsList, function (app) {
                    return {
                        value: app.displayedName,
                        uniqueName: app.name,
                        widgetable: app.widgetable
                    }
                });
                $('#appsearch').autocomplete({
                    source: apps,
                    appendTo: $("#search-list")
                }).data("autocomplete")._renderItem = function (ul, item) {                             //redefining function to add our stuff to markup
                    var $line = $("<li></li>").attr("data-unique-name", item.uniqueName);
                    $line.append($("<a>", {
                        text: item.value
                    }), $("<div></div>", {
                        class: "favourite-app-button",
                        text: "Add to favourites",
                        click: function (event) {
                            var name = $(this).parent().data("unique-name"),
                                appIndex = appNames.indexOf(name) + 1;

                            $.ajax({
                                type: "POST",
                                url: "/YOUniversity/favourite-app/add/" + appIndex,
                                success: function () {
                                    //console.log("app added to favourites successfully");
                                    $("#search-list").after($("<a>", {
                                        text: applicationsList[appIndex - 1].displayedName
                                    }).attr("href", "/YOUniversity/app/" + applicationsList[appIndex - 1].name));

                                    $(event.currentTarget).remove();           //not to allow user to do it one more time               //TODO: disable/enable these buttons
                                },
                                error: function () {
                                    alert("Sorry, error adding app to list of favourites")
                                }
                            });
                        }
                    }));
                    if (item.widgetable) {
                        $line.append($("<div></div>", {
                            class: "widget-app-button",
                            text: "Make widget for this app",
                            click: function (event) {
                                var name = $(this).parent().data("unique-name"),
                                    appIndex = appNames.indexOf(name) + 1;

                                $.ajax({
                                    type: "POST",
                                    url: "/YOUniversity/favourite-app/add-widget/" + appIndex,
                                    success: function () {
                                        //console.log("app added to widgets successfully");
                                        $("#search-list").parent().find("hr").after($("<a>", {                                //TODO: ged rid of parent() and query element by class
                                            text: applicationsList[appIndex - 1].displayedName
                                        }).attr("href", "/YOUniversity/widget/" + applicationsList[appIndex - 1].name));

                                        $(event.currentTarget).remove();             //not to allow user to do it one more time               //TODO: disable/enable these buttons
                                    },
                                    error: function () {
                                        alert("Sorry, error adding app to list of widgets")
                                    }
                                });
                            }
                        }));
                    }
                    return $line.appendTo(ul);
                }
            },
            error: function () {
                alert('error');
            }
        })
    });
</script>