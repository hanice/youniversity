
<%@ page import="com.youniversity.core.user.GroupRole; com.youniversity.core.user.User;com.youniversity.core.user.UserRole" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-user" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[GroupRole.get(groupRoleId)?.role?.authority]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ul>
                <g:each in="${userInstanceList}" status="i" var="userInstance">
				<li><g:link controller="user" action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link>
                    <g:form method="post" >
                        <g:hiddenField name="groupRoleId" value="${groupRoleId}" />
                        <g:hiddenField name="id" value="${userInstance.id}" />
                        <fieldset class="buttons">
                            <g:actionSubmit class="save" action="removeUser" value="${message(code: 'default.button.remove.label', default: 'Remove')}" />
                        </fieldset>
                    </g:form>

                </li>
				</g:each>
                <g:form method="post" >
                    <g:hiddenField name="id" value="${groupRoleId}" />
                    <fieldset class="form">
                        <g:textField name="username" id="user-search"></g:textField>
                    </fieldset>
                    <fieldset class="buttons">
                        <g:actionSubmit class="save" action="addUser" value="${message(code: 'default.button.add.label', default: 'Add')}" />
                    </fieldset>
                </g:form>
            </ul>
			<div class="pagination">
				<g:paginate total="${userInstanceTotal}" />
			</div>
		</div>
        <script>
        $(function() {
            $( "#user-search" ).autocomplete({
                    source: "/YOUniversity/group/get-all-users"
                });
            });
        </script>
	</body>
</html>
