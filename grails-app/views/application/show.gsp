
<%@ page import="com.youniversity.core.app.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-application" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-application" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list application">
			
				<g:if test="${applicationInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="application.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${applicationInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${applicationInstance?.runPath}">
				<li class="fieldcontain">
					<span id="runPath-label" class="property-label"><g:message code="application.runPath.label" default="Run Path" /></span>
					
						<span class="property-value" aria-labelledby="runPath-label"><g:fieldValue bean="${applicationInstance}" field="runPath"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${applicationInstance?.displayedName}">
				<li class="fieldcontain">
					<span id="displayedName-label" class="property-label"><g:message code="application.displayedName.label" default="Displayed Name" /></span>
					
						<span class="property-value" aria-labelledby="displayedName-label"><g:fieldValue bean="${applicationInstance}" field="displayedName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${applicationInstance?.widgetable}">
				<li class="fieldcontain">
					<span id="widgetable-label" class="property-label"><g:message code="application.widgetable.label" default="Widgetable" /></span>
					
						<span class="property-value" aria-labelledby="widgetable-label"><g:formatBoolean boolean="${applicationInstance?.widgetable}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${applicationInstance?.widgetRunPath}">
				<li class="fieldcontain">
					<span id="widgetRunPath-label" class="property-label"><g:message code="application.widgetRunPath.label" default="Widget Run Path" /></span>
					
						<span class="property-value" aria-labelledby="widgetRunPath-label"><g:fieldValue bean="${applicationInstance}" field="widgetRunPath"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:applicationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${applicationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
