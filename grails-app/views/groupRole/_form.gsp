<%@ page import="com.youniversity.core.user.GroupRole" %>



<div class="fieldcontain ${hasErrors(bean: groupRoleInstance, field: 'role', 'error')} required">
	<label for="role">
		<g:message code="groupRole.role.label" default="Role" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="role" name="role.id" from="${com.youniversity.core.user.Role.list()}" optionKey="id" optionValue="authority" required="" value="${groupRoleInstance?.role?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: groupRoleInstance, field: 'group', 'error')} required">
	<label for="group">
		<g:message code="groupRole.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="group" name="group.id" from="${com.youniversity.core.user.Group.list()}" optionKey="id" optionValue="name" required="" value="${groupRoleInstance?.group?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: groupRoleInstance, field: 'users', 'error')} ">
	<label for="users">
		<g:message code="groupRole.users.label" default="Users" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${groupRoleInstance?.users?}" var="u">
    <li><g:link controller="userRole" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="userRole" action="create" params="['groupRole.id': groupRoleInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'userRole.label', default: 'UserRole')])}</g:link>
</li>
</ul>

</div>

