<%@ page import="com.youniversity.core.app.FavouriteApp" %>



<div class="fieldcontain ${hasErrors(bean: favouriteAppInstance, field: 'application', 'error')} required">
	<label for="application">
		<g:message code="favouriteApp.application.label" default="Application" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="application" name="application.id" from="${com.youniversity.core.app.Application.list()}" optionKey="id" required="" value="${favouriteAppInstance?.application?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: favouriteAppInstance, field: 'users', 'error')} ">
	<label for="users">
		<g:message code="favouriteApp.users.label" default="Users" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: favouriteAppInstance, field: 'widget', 'error')} ">
	<label for="widget">
		<g:message code="favouriteApp.widget.label" default="Widget" />
		
	</label>
	<g:checkBox name="widget" value="${favouriteAppInstance?.widget}" />
</div>

