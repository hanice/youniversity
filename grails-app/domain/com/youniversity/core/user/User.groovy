package com.youniversity.core.user

import com.youniversity.core.app.FavouriteApp

class User {

	transient springSecurityService

	String username
	String password
        String email
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    String picturePath

    static hasMany = [favouriteApps:FavouriteApp]
        
	static constraints = {
		username blank: false, unique: true
		password blank: false
        picturePath nullable: true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<GroupRole> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.groupRole } as Set
	}

	def beforeInsert() {

	}

	def beforeUpdate() {

	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
