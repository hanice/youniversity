package com.youniversity.core.user

class GroupRole {
    
    Role role
    Group group
    String authority
    
    String getAuthority()   {
        if("ROLE_"+group?.name+"_"+role?.authority!=authority)
            authority="ROLE_"+group?.name+"_"+role?.authority
        return authority

    }

    void setAuthority(String value) {}
    
    static hasMany = [users:UserRole,permissions:Permission]
    static belongsTo = Permission
    
    static constraints = {
        role(nullable:false)
        group(nullable:false)
    }

}
