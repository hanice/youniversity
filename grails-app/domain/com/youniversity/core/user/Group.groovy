package com.youniversity.core.user

class Group {
    String name
    String description
    Group parent
    String picturePath

    static hasMany = [groupRoles:GroupRole]
    
    static constraints = {
        name blank: false
        description nullable:true, blank: true
        parent nullable: true
        picturePath nullable: true

    }
    
    static mapping =    {
        table "group_table"
    }
}
