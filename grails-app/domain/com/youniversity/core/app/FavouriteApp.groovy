package com.youniversity.core.app
import com.youniversity.core.user.User

class FavouriteApp {
    Application application
    boolean widget
    User user

    static constraints = {
    }

    static mapping = {
        user nullable:false
        application lazy: false
    }
}
