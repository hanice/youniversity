
<%@ page import="com.youniversity.apps.chat.Message" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-message" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list message">
			
				<g:if test="${message?.lastEdited}">
				<li class="fieldcontain">
					<span id="lastEdited-label" class="property-label"><g:message default="Last Edited" /></span>
					
						<span class="property-value" aria-labelledby="lastEdited-label"><g:formatDate date="${message?.lastEdited}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${message?.date}">
				<li class="fieldcontain">
					<span id="date-label" class="property-label"><g:message code="message.date.label" default="Date" /></span>
					
						<span class="property-value" aria-labelledby="date-label"><g:formatDate date="${message?.date}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${message?.editable}">
				<li class="fieldcontain">
					<span id="editable-label" class="property-label"><g:message code="message.editable.label" default="Editable" /></span>
					
						<span class="property-value" aria-labelledby="editable-label"><g:formatBoolean boolean="${message?.editable}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${message?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="message.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${message}" field="text"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:message, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${message}"><g:message default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="Delete" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
