<%@ page import="com.youniversity.apps.chat.Message" %>



<div class="fieldcontain ${hasErrors(bean: message, field: 'lastEdited', 'error')} ">
	<label for="lastEdited">
		<g:message code="message.lastEdited.label" default="Last Edited" />
		
	</label>
	<g:datePicker name="lastEdited" precision="day"  value="${message?.lastEdited}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: message, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="message.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="day"  value="${message?.date}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: message, field: 'editable', 'error')} ">
	<label for="editable">
		<g:message code="message.editable.label" default="Editable" />
		
	</label>
	<g:checkBox name="editable" value="${message?.editable}" />
</div>

<div class="fieldcontain ${hasErrors(bean: message, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="message.text.label" default="Text" />
		
	</label>
	<g:textField name="text" value="${message?.text}"/>
</div>

