
<%@ page import="com.youniversity.apps.chat.Message" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-message" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="lastEdited" title="${message(code: 'message.lastEdited.label', default: 'Last Edited')}" />
					
						<g:sortableColumn property="date" title="${message(code: 'message.date.label', default: 'Date')}" />
					
						<g:sortableColumn property="editable" title="${message(code: 'message.editable.label', default: 'Editable')}" />
					
						<g:sortableColumn property="text" title="${message(code: 'message.text.label', default: 'Text')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${messageList}" status="i" var="message">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${message.id}">${fieldValue(bean: message, field: "lastEdited")}</g:link></td>
					
						<td><g:formatDate date="${message.date}" /></td>
					
						<td><g:formatBoolean boolean="${message.editable}" /></td>
					
						<td>${fieldValue(bean: message, field: "text")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${messageCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
