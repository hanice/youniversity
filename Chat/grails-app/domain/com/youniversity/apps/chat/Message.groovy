package com.youniversity.apps.chat


class Message {

    String text
    Date date
    Date lastEdited
    Boolean editable


    static constraints = {
        lastEdited nullable: true
    }
}
