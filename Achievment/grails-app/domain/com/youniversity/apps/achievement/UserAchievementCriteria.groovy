package com.youniversity.apps.achievement


class UserAchievementCriteria {
    Integer user
    AchievementCriteria criteria
    Boolean done

    static constraints = {
        criteria nullable: true
    }
}
