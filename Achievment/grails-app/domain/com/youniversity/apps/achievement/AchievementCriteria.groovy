package com.youniversity.apps.achievement

import javax.print.AttributeException

class AchievementCriteria {
    Achievement achievement
    String attribute
    String value
    int importance


    static constraints = {
        attribute nullable:true
        value nullable:true
    }
}
