package com.youniversity.apps.untitled1.tes
//import com.Todo
import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class TodoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def permissions = [

    ]
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [ todoList:Todo.list(params), todoCount: Todo.count()]
    }

    def show(Todo todo) {
        [todo:todo]
    }

    def create() {        
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todo) {
        if(todo.hasErrors()) {
            render(view: "create", model: [todo: todo])
        }
        else {
            todo.save flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todo.id])
                    redirect todo
                }
                '*' { respond todo, [status: CREATED] }
            }
        }
    }

    def edit(Todo todo) { 
        [todo:todo]
    }

    @Transactional
    def update(Todo todo) {
        if(todo == null) {
            render status:404
        }
        else if(todo.hasErrors()) {
            render(view: "edit", model: [todo: todo])
        }
        else {
            todo.save flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todo.id])
                    redirect todo 
                }
                '*'{ respond todo, [status: OK] }
            }
        }        
    }

    @Transactional
    def delete(Todo todo) {
        if(todo) {
            todo.delete flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todo.id])
                    redirect action:"index", method:"GET" 
                }
                '*'{ render status: NO_CONTENT }
            }                
        }
        else {
            render status: NOT_FOUND
        }
    }
}

