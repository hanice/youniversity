package com.youniversity.apps.untitled1.tes

class Todo {
    String text
    static constraints = {
        text nullable:false, blank:false
    }
}
