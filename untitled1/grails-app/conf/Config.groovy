// configuration for plugin testing - will not be included in the plugin zip

log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}
    appenders {
        rollingFile name:"errorLog", maxFileSize:"5MB", maxBackupIndex: 10, file:"/home/richard/NetBeansProjects/YOUniversity/grails-app/error.log", 'append':true, threshold:org.apache.log4j.Level.WARN
        rollingFile name:"debugLog", maxFileSize:"5MB", maxBackupIndex: 10, file:"/home/richard/NetBeansProjects/YOUniversity/grails-app/all.log", 'append':true, threshold:org.apache.log4j.Level.INFO
        console name:'stdout', threshold:org.apache.log4j.Level.WARN
    }
    root {
        info "debugLog", "errorLog", "stdout"
    }
    debug 'grails.app'


    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    info 'grails.app'
}
