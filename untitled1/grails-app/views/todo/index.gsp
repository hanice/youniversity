
<%@ page import="com.youniversity.apps.untitled1.tes.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
            ${grailsApplication.getClassForName("com.youniversity.core.user.User").get(grailsApplication.getClassForName("com.youniversity.apps.achievement.UserAchievementCriteria").get(1).user).username}
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="text" title="${message(code: 'todo.text.label', default: 'Text')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${todoList}" status="i" var="todo">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link action="show" id="${todo.id}">${fieldValue(bean: todo, field: "text")}</g:link></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
